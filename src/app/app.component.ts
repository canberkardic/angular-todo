import {Component} from '@angular/core';
import {Todo} from './models/Todo';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'todoapp';
  /*Todoo tipinde ItemList oluşturuluyor.*/
  ItemList: Todo[] = [];
  Item: Todo = {Completed: false, ItemName: ''};
  /* : işareti TS'de tip tanımlamada kullanılır.*/
  AddItem() {
    /*Oluşturulan itemlist içine todoo modeli türünde bir obje girildi ve bu objenin fieldları yukarıdaki oluşturulan nesneden alındı.*/
    this.ItemList.push({'Completed': this.Item.Completed, 'ItemName': this.Item.ItemName});
    this.Item.ItemName = ' ';
    console.log(this.ItemList);
  }

  completeTodo(todo: Todo) {
    /*Todoo olarak belirlenen nesnenin tamamlandığını belirtmek için bu fonksiyon parametre olarak bir */
    this.ItemList.find(x => x.ItemName === todo.ItemName).Completed = true;
    console.log(this.ItemList);
  }

  delete(todo: Todo) {
    /**/
    const index = this.ItemList.indexOf(todo);
    this.ItemList.splice(index, 1);
    return false;
  }
}

