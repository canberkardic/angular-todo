## Angular 6 Todo App
A basic application todo application that created with AngularCLI, Bootstrap and 
fontawesome.

Includes;
- Angular 6
- Fontawesome 5
- Bootstrap 4

### How to use it?
 
You can add todos with using add button on the left bottom and can delete todos <br>
using delete button that's located 
on the right of a todo.